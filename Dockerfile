FROM maven:3.6.3-openjdk-11

WORKDIR /usr/src/app

COPY target/scheduler-0.0.1-SNAPSHOT.jar /usr/src/app

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "java -jar scheduler-0.0.1-SNAPSHOT.jar" ]
