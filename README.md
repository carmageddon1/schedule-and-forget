### Schedule and Forget!

Launch the docker-compose.yaml by running 'docker-compose up' which should execute the SQL script to initialize the database table for scheduled tasks.
THEN launch the maven application! It will resume executing tasks in case application was interrupted.