package com.genadi.rest.service;

import javax.sql.DataSource;

import com.github.kagkarlsson.scheduler.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Data;

@Data
@Service
public class SchedulingService {
    private final Scheduler scheduleExecutor;
    private final DataSource dataSource;

    @Autowired
    public SchedulingService(final Scheduler executor, DataSource dataSource) {
        this.scheduleExecutor = executor;
        this.dataSource = dataSource;
    }
}
