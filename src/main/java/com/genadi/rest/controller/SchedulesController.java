package com.genadi.rest.controller;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import com.genadi.configuration.TaskConfiguration;
import com.genadi.model.Schedule;
import com.genadi.rest.service.SchedulingService;
import com.github.kagkarlsson.scheduler.ScheduledExecution;
import com.github.kagkarlsson.scheduler.task.TaskInstanceId;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchedulesController {

	@Autowired
	private SchedulingService schedulingService;

	@PostMapping("/schedules")
	public ResponseEntity<Schedule> createSchedule(@Valid @RequestBody Schedule schedule) {
		var schedulerInstance = this.schedulingService.getScheduleExecutor();
		schedule.setId(UUID.randomUUID().toString());

		var timeToRun = Instant.parse(schedule.getTimestamp());
		var taskToChedule = Tasks.oneTime(TaskConfiguration.TASK_NAME, Schedule.class).execute((inst, ctx) -> {
		});
		var task = taskToChedule.instance(schedule.getId(), schedule);

		schedulerInstance.schedule(task, timeToRun);

		return ResponseEntity.status(HttpStatus.ACCEPTED).body(schedule);
	}

	@PutMapping("/schedules/{id}")
	public ResponseEntity<Schedule> updateSchedule(@PathVariable("id") String id, @RequestBody Schedule schedule) {
		var taskInstance = TaskInstanceId.of(TaskConfiguration.TASK_NAME, id);
		var schedulerInstance = this.schedulingService.getScheduleExecutor();
		var scheduledTask = schedulerInstance.getScheduledExecution(taskInstance);
		
		if (scheduledTask.isPresent() && id.equalsIgnoreCase(schedule.getId())) {
			Instant timeToRun = Instant.parse(schedule.getTimestamp());
			schedulerInstance.reschedule(taskInstance, timeToRun, schedule);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Schedule.builder().id(id)
					.errorMessage("Resource not found, or ID does not match to query").build());
	}

	@GetMapping("/schedules/{id}")
	public ResponseEntity<Schedule> getSchedule(@PathVariable("id") String id) {
		TaskInstanceId taskInstance = TaskInstanceId.of(TaskConfiguration.TASK_NAME, id);
		var schedulerInstance = this.schedulingService.getScheduleExecutor()
				.getScheduledExecution(taskInstance);

		if (schedulerInstance.isPresent()) {
			Schedule schedule = (Schedule) schedulerInstance.get().getData();
			return ResponseEntity.status(HttpStatus.OK).body(schedule);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(Schedule.builder().id(id).errorMessage("No scheduled Task resource found").build());
		}
	}

	@DeleteMapping("/schedules/{id}")
	public ResponseEntity<Schedule> deleteSchedule(@PathVariable("id") String id) {
		var schedulerInstance = this.schedulingService.getScheduleExecutor();
		TaskInstanceId taskInstance = TaskInstanceId.of(TaskConfiguration.TASK_NAME, id);
		Optional<ScheduledExecution<Object>> scheduledTask = schedulerInstance.getScheduledExecution(taskInstance);

		if (scheduledTask.isPresent()) {
			schedulerInstance.cancel(taskInstance);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(Schedule.builder().id(id).errorMessage("No scheduled Task resource found").build());
	}

}