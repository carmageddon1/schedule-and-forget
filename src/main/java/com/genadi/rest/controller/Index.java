package com.genadi.rest.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class Index {

	/**
	 * Heartbeat static value
	 */
	private static final String HEALTHCHECK = "I am still alive";

	@GetMapping("/")
	public String heartbeat() {
		return HEALTHCHECK;
	}
}