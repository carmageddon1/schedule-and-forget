package com.genadi.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Schedule implements Serializable {
    @Size(max = 36, message = "ID should be a UUID in the form of 8-4-4-4-12")
    private String id;
    @NotNull
    private String message;
    @NotNull
    private String timestamp;
    private String errorMessage;
}
