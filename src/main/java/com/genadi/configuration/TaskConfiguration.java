/**
 * Copyright (C) Gustav Karlsson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.genadi.configuration;

import com.genadi.model.Schedule;
import com.github.kagkarlsson.scheduler.task.Task;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TaskConfiguration {
    private static final Logger log = LoggerFactory.getLogger(TaskConfiguration.class);
    public static final String TASK_NAME = "scheduled_message";


    /**
     * Define a one-time task which have to be manually scheduled.
     */
    @Bean
    Task<Schedule> scheduledTask() {
        return Tasks.oneTime(TASK_NAME, Schedule.class)
            .execute((instance, ctx) -> {
                log.info(instance.getData().getMessage());
            });
    }

}
