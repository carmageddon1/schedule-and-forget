package com.genadi.rest.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.time.Instant;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.genadi.model.Schedule;
import com.genadi.rest.controller.configuration.H2TestProfileConfig;
import com.genadi.rest.service.SchedulingService;
import com.github.kagkarlsson.scheduler.ScheduledExecution;
import com.github.kagkarlsson.scheduler.Scheduler;
import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.kagkarlsson.scheduler.task.TaskInstanceId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@Sql({"/schema.sql"})
@RunWith(SpringRunner.class)
@WebMvcTest(value = {SchedulesController.class, H2TestProfileConfig.class})
public class ScheduleRESTTests {

	@Autowired
	private SchedulesController schedulesController;
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private SchedulingService schedulingServiceMock;
	private ScheduledExecution scheduledExecutionTaskMock;
	private Scheduler schedulerTaskMock;
	private ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void contextLoads() {
		assertThat(schedulesController).isNotNull();
	}

	@Test
	public void canCreateNewSchedule() throws Exception {
		var expectedSchedule = Schedule.builder().message("Print This Scheduled Message Please!")
				.timestamp("2020-11-13T07:25:12.066Z").build();

		ObjectMapper objectMapper = new ObjectMapper();
		mockedObjectsSetup(expectedSchedule);

		this.mockMvc.perform(post("/schedules").
		content(objectMapper.writeValueAsString(expectedSchedule))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());

		verify(schedulerTaskMock, atLeastOnce()).schedule(Mockito.<TaskInstance<Schedule>>any(), any(Instant.class));

	}

	@Test
	public void canRetrieveSchedule() throws Exception {
		var expectedSchedule = Schedule.builder().message("Print This Scheduled Message Please!")
				.timestamp("2020-11-13T07:25:12.066Z").id("1").build();
		mockedObjectsSetup(expectedSchedule);

		this.mockMvc.perform(get("/schedules/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id", is("1")));
		verify(this.scheduledExecutionTaskMock, atLeastOnce()).getData();
	}

	@Test
	public void canUpdateExistingSchedule() throws JsonProcessingException, Exception {
		var expectedSchedule = Schedule.builder()
										.message("Print This Revised Message Please!")
										.timestamp("2025-11-13T07:25:12.066Z")
										.id("1")
										.build();
		mockedObjectsSetup(expectedSchedule);

		this.mockMvc.perform(put("/schedules/1").content(objectMapper.writeValueAsString(expectedSchedule))
												.contentType(MediaType.APPLICATION_JSON))
							.andExpect(status().isNoContent());
		verify(schedulerTaskMock, atLeastOnce()).reschedule(Mockito.<TaskInstance<Schedule>>any(), any(Instant.class), any(Schedule.class));
		
	}

	@Test
	public void canDeleteExistingSchedule() throws Exception {
			var scheduleToDelete = Schedule.builder()
											.message("Print This Revised Message Please!")
											.timestamp("2025-11-13T07:25:12.066Z")
											.id("1")
											.build();
			mockedObjectsSetup(scheduleToDelete);

			this.mockMvc.perform(delete("/schedules/1"))
						.andExpect(status().isNoContent());
			verify(this.schedulerTaskMock, atLeastOnce()).cancel(any(TaskInstanceId.class));
	}
	
	@Test
	public void canNotUpdateWrongInformationSchedule() throws Exception {
			var scheduleToDelete = Schedule.builder()
											.message("Print This Revised Message Please!")
											.timestamp("2025-11-13T07:25:12.066Z")
											.id("2")
											.build();
			mockedObjectsSetup(scheduleToDelete);

			this.mockMvc.perform(put("/schedules/1")
									.content(objectMapper.writeValueAsString(scheduleToDelete))
									.contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isNotFound());
			verify(schedulerTaskMock, never()).reschedule(Mockito.<TaskInstance<Schedule>>any(), any(Instant.class), any(Schedule.class));
	}
	
	@Test
	public void canNotDeleteWrongSchedule() throws Exception {
		var scheduleToDelete = Schedule.builder()
										.message("Print This Revised Message Please!")
										.timestamp("2025-11-13T07:25:12.066Z")
										.id("1")
										.build();
		mockedObjectsSetup(scheduleToDelete);
		Mockito.reset(schedulerTaskMock);
		when(schedulerTaskMock.getScheduledExecution(any(TaskInstanceId.class))).thenReturn(Optional.empty());


		this.mockMvc.perform(delete("/schedules/1"))
		.andExpect(status().isNotFound());
	}
	
	private Scheduler mockedObjectsSetup(Schedule schedule) {
		scheduledExecutionTaskMock = mock(ScheduledExecution.class);
		schedulerTaskMock = mock(Scheduler.class);

		when(scheduledExecutionTaskMock.getData()).thenReturn(schedule);
		when(schedulerTaskMock.getScheduledExecution(any(TaskInstanceId.class))).thenReturn(Optional.of(scheduledExecutionTaskMock));
		when(schedulingServiceMock.getScheduleExecutor()).thenReturn(schedulerTaskMock);

		
		return schedulerTaskMock;
	}
}